import React from 'react';
import { Button, Container, LogoTop, SubTitle, Title, IlustrationMain, TextContainer } from './Styles';
import { AntDesign } from '@expo/vector-icons';

import logo from '../../assets/logo.png';
import main from '../../assets/ilustracao-a.png'


export default function StartPage() {
  return (
      <Container>
        <LogoTop source={logo}/>
        <IlustrationMain source={main} />

        <TextContainer>
          <Title>
            O melhor lugar para encontrar o que você  procura.
          </Title>
          <SubTitle>
            Aqui você pode comprar de forma rápida e segura,
            de onde você estivar e com apenas alguns toques no seu celular.
          </SubTitle>
        </TextContainer>
        <Button><AntDesign name="arrowright" size={24} color="whith" /></Button>

      </Container>
  );
}
