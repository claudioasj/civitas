import styled from 'styled-components/native';

export const Container = styled.View`
    flex : 1;
    background-color : #E5E5E5;
    padding : 0 10px;
    align-items : center;
`
//imagens
export const LogoTop = styled.Image`
    margin: 20% auto;
`

export const IlustrationMain = styled.Image`
    margin-right: -25%;
`
// texto
export const TextContainer = styled.View`
  margin: 3% 0;
`
export const Title = styled.Text`
    font-size: 20px;
    text-align : left;
`
export const SubTitle = styled.Text`
    font-size: 15px;
    text-align : left;
`
export const Button = styled.TouchableOpacity`
    position: relative;
    left: 25%;
    text-align : right;
    padding: 5px 7px 5px 50px;
    background-color: rgb(56, 167, 229);
    border-radius: 25px;
`