import styled from "styled-components/native";

export const Background = styled.View`
    flex: 1;
    align-items: center;
    background-color: #e5e5e5;
`
export const LogoTop = styled.Image`
    width: 140px;
    height: 140px;
    margin-top: -70px;
    margin-bottom: 75px;
`

export const Container = styled.View`
    background-color: #fff;
    border-top-left-radius: 45px;
    border-top-right-radius: 45px;

    width: 100%;
    height: 90%;
    margin-top: 140px;
    align-items: center;
`

export const InputData = styled.TextInput`
    background-color: #b8b8b8;
    width: 70%;
    margin: 3% 0;
    border-radius: 25px;
    height: 40px;
    padding: 10px;
    font-size: 20px;
`

export const Btn = styled.TouchableOpacity`
    text-align : center;
    margin-top: 30px;
    padding: 10px 50px;
    background-color: rgb(56, 167, 229);
    border-radius: 25px;
`
    
export const Entrar = styled.Text`
    font-size: 35px;
    color: #fff;
`