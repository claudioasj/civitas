import React from "react";
import { Text} from "react-native";
import { Container, InputData, Background, Btn, LogoTop, Entrar} from "./Styles";

import logo from "../../assets/logoIcon.png"


export default function Login() {
    return (
        <Background>
        <Container>
            <LogoTop source={logo} />
            <InputData placeholder={"Username"}/>
            <InputData placeholder={"Senha"} secureTextEntry={true}/> 
            <Text>Esqueci minha Senha ou meu Username</Text>
            <Btn><Entrar>Entrar</Entrar></Btn>
        </Container>
        </Background>
    )
}