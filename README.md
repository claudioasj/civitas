# Civitas
- Este repositório foi desenvolvido para a tarefa opcional do TT da EJCM:
	- *Estilizar o Civitas de acordo com a prototipagem que você fez na aula de UI*
## Ferramentas no Projeto:
<p align="center" >
<img src="https://icons-for-free.com/iconfiles/png/512/expo-1324440155568384208.png" width="40"> Expo <br>
<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/a/a7/React-icon.svg/2300px-React-icon.svg.png" width="40">
React-Native<br>
Styled Components
</p>

## O que eu fiz
- Primeiramente eu criei as pastas e organizei os arquivos da forma que foi explicada no treinamento.
- Criando a pagina de start, basicamente, no meu projeto do figma ao abrir o aplicativo ele mostra uma dela e chamei ela de `StartPage.tsx` e ficou assim:
![print da tela de inicio](./assets/printStart.jpeg)
- Depois fiz a pagina de login (`Login.tsx`) que ficou dessa fora:
 ![print da tela de login](./assets/printLogin.jpeg)

- <img src="https://upload.wikimedia.org/wikipedia/commons/3/33/Figma-logo.svg" width="10"> : https://www.figma.com/file/POKcQEiY9GSQespZy7xDqf/civitas?node-id=59%3A215
> Uma Falha:  Não fiz a animação por falta de tempo mas pretendo depois do TT finalizar o meu civitas por inteiro e nele eu aplico no styled components colocar o css animation na pagina de inicio e arrumar a proporção do button. Além disso, eu não usei react navegation pra mudar de pagina sem precisar ir no `app.tsx` e colocar a pagina que deve ser renderizada.
  

Por fim, é isso, queria informalmente "dizer" que não so evolui muito mas também acabei me divertindo criando o civitas e todas as aplicações dele. Espero aprender o suficiente pra ajudar a EJCM em seus projetos e ir com a galera para Maceió.
### <p>OSS!</p>
![BMO](https://i.pinimg.com/originals/e5/93/ab/e593ab0589d5f1b389e4dfbcce2bce20.gif)